# cron job to run flashlight stop every evening at 19:00 GMT
resource "aws_cloudwatch_event_rule" "cron-flashlight-event-rule-1" {
  name                = "cron-flashlight"
  description         = "Stop flashlight instances nightly"
  schedule_expression = "cron(0 19 * * ? *)"
}

resource "aws_cloudwatch_event_target" "cron-flashlight-event-target-1" {
  arn  = aws_lambda_function.flashlight_stop.arn
  rule = aws_cloudwatch_event_rule.cron-flashlight-event-rule-1.name
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_flashlight_stop_lambda" {
  action        = "lambda:InvokeFunction"
  function_name = "flashlight_stop"
  principal     = "events.amazonaws.com"
  statement_id  = "AllowExecutionFromCloudWatch"
}